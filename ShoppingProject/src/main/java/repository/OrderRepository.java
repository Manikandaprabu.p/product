package repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Entity.order;

@Repository
public interface OrderRepository extends JpaRepository<order,Integer> {

	
}
