package repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Integer> {

	

	Customer getCustomerByEmailAndName(String email, String name);


}


