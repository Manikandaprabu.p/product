package Entity;

//import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="product_details1")
public class Product {

 

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  
//  @Column(name= "P_ID")
  private int id;
  
//  @Column(name= "P_Name")
  private String name;
  
//  @Column(name= "P_availableQuantity")
  private int availableQuantity;
  
//  @Column(name= "P_price")
  private float price;

  public int getId() {
      return id;
  }

  public void setId(int id) {
      this.id = id;
  }

  public String getName() {
      return name;
  }

  public void setName(String name) {
      this.name = name;
  }

  public int getAvailableQuantity() {
      return availableQuantity;
  }

  public void setAvailableQuantity(int availableQuantity) {
      this.availableQuantity = availableQuantity;
  }

  public float getPrice() {
      return price;
  }

  public void setPrice(float price) {
      this.price = price;
  }

  @Override
  public String toString() {
      return "Product{" +
              "id=" + id +
              ", name='" + name + '\'' +
              ", availableQuantity=" + availableQuantity +
              ", price=" + price +
              '}';
  }

	public Product() {
		super();
	}

	public Product(int id, String name, int availableQuantity, float price) {
		super();
		this.id = id;
		this.name = name;
		this.availableQuantity = availableQuantity;
		this.price = price;
	}
  
}

