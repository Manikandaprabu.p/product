package Entity;

//import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.ToString;



/*@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
*/
@ToString
@Entity
@Table(name="Shopping_cart")
public class ShoppingCart {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  
//  @Column(name= "C_ID")
  private int id;
  
//  @Column(name= "P_ID")
  private int productId;
  
//  @Column(name= "P_Name")
  private String productName;
  
//  @Column(name= "P_quantity")
  private int quantity;
  
//  @Column(name= "P_amount")
  private float amount;

  public ShoppingCart() {
  }

  public ShoppingCart(int productId, String productName, int quantity, float amount) {
      this.productId = productId;
      this.productName = productName;
      this.quantity = quantity;
      this.amount = amount;
  }

  public ShoppingCart(int productId, int quantity) {
      this.productId = productId;
      this.quantity = quantity;
  }

  public int getId() {
      return id;
  }

  public void setId(int id) {
      this.id = id;
  }

  public int getProductId() {
      return productId;
  }

  public void setProductId(int productId) {
      this.productId = productId;
  }

  public String getProductName() {
      return productName;
  }

  public void setProductName(String productName) {
      this.productName = productName;
  }

  public int getQuantity() {
      return quantity;
  }

  public void setQuantity(int quantity) {
      this.quantity = quantity;
  }

  public float getAmount() {
      return amount;
  }

  public void setAmount(float amount) {
      this.amount = amount;
  }

  @Override
  public String toString() {
      return "ShoppingCart{" +
              "id=" + id +
              ", productId=" + productId +
              ", productName='" + productName + '\'' +
              ", quantity=" + quantity +
              ", amount=" + amount +
              '}';
  }
}
